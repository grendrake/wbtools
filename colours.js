var dropZone = document.getElementById('dropZone');
var outputZone = document.getElementById('output');
var fileSelector = document.getElementById('fileSelector');
var statusLine = document.getElementById('statusLine');
var totalArea = document.getElementById('totalArea');

// Optional.   Show the copy icon when dragging over.  Seems to only work for chrome.
dropZone.addEventListener('dragover', function(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
});

// Get file data on drop
dropZone.addEventListener('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();
    fileSelected(e.dataTransfer.files); // Array of all files
});
fileSelector.addEventListener('change', function(e) {
    var files = document.getElementById('fileSelector').files;
    fileSelected(files);
});

function fileSelected(files) {
    fileSelector.innerHTML = "";
    statusLine.textContent = "Loading data... (this may take a while)";
    for (var i=0, file; file=files[i]; i++) {
        if (file.type.match(/image.*/)) {
            var reader = new FileReader();
            reader.onload = finishedLoading;
            reader.readAsDataURL(file); // start reading the file data.
            break;
        }
    }
}

function finishedLoading( e2 ) {
    var img = document.createElement('img');
    img.src = e2.target.result;

    var counts = {};
    var total_count = 0;

    const startTime = performance.now();

    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const context = canvas.getContext("2d");
    context.drawImage(img, 0, 0, img.width, img.height);

    for (var y = 0; y < img.height; ++y) {
        for (var x = 0; x < img.width; ++x) {
            const pixelData = context.getImageData(x, y, 1, 1).data;
            const key = pixelData[0] + ", " + pixelData[1] + ", " + pixelData[2];
            if (counts.hasOwnProperty(key)) {
                ++counts[key].count;
            } else {
                counts[key] = { colour: pixelData, count: 1 };
            }
            ++total_count;
        }
    }

    const midTime = performance.now();

    var result = "<table><th></th><th>Colour</th><th>Count</th><th>Percent</th><th>Area</th></tr>";

    const taTemp = totalArea.value.replace(/[, ]/g, "");
    totalAreaResult = parseFloat(taTemp);
    if (totalAreaResult != taTemp) {
        const errorDiv = document.createElement("div");
        errorDiv.classList.add("error");
        errorDiv.innerText = "Invalid map area.";
        outputZone.appendChild(errorDiv);
        return;
    }

    for (var key in counts) {
        const percent = (counts[key].count / total_count * 100);
        const area = Math.round(Math.round((totalAreaResult * (percent / 100)) * 100) / 100);

        result += "<tr><td style='background-color:rgb(" + key;
        result += ")'>&nbsp;&nbsp;</td><td>"+ key + "</td><td>";
        result += counts[key].count + "</td><td>";
        result += (Math.round(percent * 100) / 100);
        result += "%</td><td>" + area.toLocaleString();
        result += "</td></tr>";
    }
    result += "</table><br>";

    outputZone.innerHTML = result;
    img.width = 400;
    outputZone.appendChild(img);
    const endTime = performance.now();

    statusLine.textContent = "Done! Calculation: " + (midTime - startTime) +
        " ms; Output: " + (endTime - midTime) + " ms";
}
